var canvas = document.getElementById('canvas');
var context = canvas.getContext('2d');
var dwn = document.getElementById('btndownload');
var redo =document.getElementById('redo');
var undo =document.getElementById('undo');
var textBox =document.getElementById('textBox');
var triang =document.getElementById('triang');
var re =document.getElementById('rec');
var circle =document.getElementById('circle');

var state = context.getImageData(0, 0, canvas.width, canvas.height);
window.history.pushState(state, null);

var font_size;
var font_spe;

context.lineWidth = 5;
var down =false;
var xPos;
var yPos;
var tools ='pencil';
var shape_x;
var shape_y;
function changeFont(n)
{
    font_spe = n.value;
    context.font =font_size+" " +font_spe ;
}

function fontSz(n)
{
    var font_size = n.value +'px';
    
     context.font =font_size+" "+font_spe ;
} 

canvas.addEventListener('mousemove',function(e)
{
    if(tools == 'pencil')
    {
        draw(e);
        canvas.style.cursor='url(pencil1.png), auto';
    }
    if(tools == 'erase')
    {
        eraser(e);
        canvas.style.cursor='url(y2.png), auto';
    }
    if(tools == 'triango')
    canvas.style.cursor='url(t2.png), auto';
    if(tools == 'triango' && down == true)
    {
        shape_x = e.offsetX;
        shape_y = e.offsetY;
        context.clearRect(0, 0, canvas.width, canvas.height);
        context.globalCompositeOperation="source-over";
        context.putImageData(trian, 0, 0);

        context.beginPath();
        context.lineWidth = 8;
        var top = (xPos + shape_x) / 2;
        context.moveTo(shape_x, shape_y);
        context.lineTo(top, yPos);
        context.lineTo(xPos,shape_y);
        context.closePath();
        context.stroke();
    }
    if(tools == 'circle' && down == true)
    {
        shape_x = e.offsetX;
        shape_y = e.offsetY;
        context.clearRect(0, 0, canvas.width, canvas.height);
        context.globalCompositeOperation="source-over";
        context.putImageData(circl, 0, 0);

        context.beginPath();
        context.lineWidth = 8;
        var shape_x = (xPos + shape_x) / 2;
        var shape_y = (yPos + shape_y) / 2;
        var r = Math.abs(xPos - shape_x);
        var s = Math.abs((yPos - shape_y)/r);
    
        context.arc(shape_x, shape_y, r, 0, 2*Math.PI);
        context.closePath();
        context.stroke();
    }
    if(tools =='text')
    canvas.style.cursor='url(A2.png), auto';
    if(tools =='Rectangular')
    canvas.style.cursor='url(82.png), auto';
    if(tools =='circle')
    canvas.style.cursor='url(72.png), auto';
    if(tools == 'Rectangular' && down == true)
    {
        shape_x = e.offsetX;
        shape_y = e.offsetY;
        context.clearRect(0, 0, canvas.width, canvas.height);
        context.globalCompositeOperation="source-over";
        context.putImageData(rec, 0, 0);
        context.beginPath();
        context.lineWidth = 8;
        if(shape_x < xPos && shape_y < yPos) {
            context.rect(shape_x, shape_y, xPos - shape_x, yPos - shape_y);
        }
        else if(shape_x < xPos) {
            context.rect(shape_x, yPos, xPos - shape_x, shape_y - yPos);
        }
        else if(shape_y < yPos) {
            context.rect(xPos, shape_y, shape_x - xPos, yPos - shape_y);
        }
        else {
            context.rect(xPos, yPos, shape_x - xPos, shape_y - yPos);
        }
        context.stroke();
        context.closePath();
    }

});
dwn.onclick = function(){
    download(canvas, 'myimage.png');
}

function download(canvas, filename) {
    var lnk = document.createElement('a');
    lnk.download = filename;
    lnk.href = canvas.toDataURL("image/png;base64");
    if (document.createEvent) {
      e = document.createEvent("MouseEvents");
      e.initMouseEvent("click", true, true, window,
                       0, 0, 0, 0, 0, false, false, false,
                       false, 0, null);
      lnk.dispatchEvent(e);
    } else if (lnk.fireEvent) {
      lnk.fireEvent("onclick");
    }
  }


var trian =new Image();
var rec =new Image();
var circl =new Image();

canvas.addEventListener('mousedown',function(e)
{
    down = true;
    context.beginPath();
    xPos = e.offsetX;
    yPos = e.offsetY;
    context.moveTo(xPos,yPos);    
    if(tools =='text')
    {
        textBox.style = "visibility:visible";
        var posx = e.pageX +'px';
        var posy = e.pageY +'px';
        textBox.style.left = posx;
        textBox.style.top = posy;
        textBox.onkeydown = ifEnter;
    }
    if(tools =='triango')
    {
        trian = context.getImageData(0, 0, canvas.width, canvas.height);
        
     };
        delete trian;
    if(tools =='Rectangular')
    {
       rec = context.getImageData(0, 0, canvas.width, canvas.height);
            
    };
     delete rec;
     if(tools =='circle')
    {
       circl = context.getImageData(0, 0, canvas.width, canvas.height);
            
    };
     delete circl;
});
function ifEnter(e)
{
    if(e.keyCode == 13)
    {
        context.fillText(textBox.value, parseInt(this.style.left ,10)- canvas.offsetLeft, parseInt(this.style.top,10)- canvas.offsetTop);
        textBox.value = "";
        textBox.style = "visibility:hidden";
    }
}
canvas.addEventListener('mouseout', function () {
    down = false;
    context.beginPath();
});

canvas.addEventListener('mouseup',function(){ 
    down = false;
    var state = context.getImageData(0, 0, canvas.width, canvas.height);
    window.history.pushState(state, null);
});

function draw(e)
{
    
    tools = 'pencil';
    if(down == true)
    {
        context.globalCompositeOperation = "source-over";
        xPos = e.offsetX;
        yPos = e.offsetY;
        context.lineTo(xPos,yPos);
        context.stroke();
    }
}

function eraser(e)
{

    tools = 'erase';
    if(down == true)
    {
        xPos = e.offsetX;
        yPos = e.offsetY;
        context.globalCompositeOperation = "destination-out";
        context.lineTo(xPos,yPos);
        context.stroke();
    }
}

color.onchange = function(e) { context.strokeStyle = e.srcElement.value }
function changeColor(color){ context.strokeStyle = color; }
function clearCanvas() { context.clearRect(0,0,canvas.width,canvas.height); }
function changeSize(size) { context.lineWidth = size; }
function triggerClick(openFile) { document.getElementById('file').click(); }
document.getElementById('colorse').onclick = function() {document.getElementById('color').click();}


redo.addEventListener('click', function(){
    window.history.go(+1);
}, false);

triang.addEventListener('click', function(){
    tools = 'triango';
}, false);
re.addEventListener('click', function(){
    tools = 'Rectangular';
}, false);
circle.addEventListener('click', function(){
    tools = 'circle';
}, false);

text.addEventListener('click', function(){
   tools ='text';
}, false);

undo.addEventListener('click', function(){
    window.history.go(-1);
}, false);

window.addEventListener('popstate', changeStep, false);

function changeStep(e){
  context.clearRect(0, 0, canvas.width, canvas.height);
  if(e.state){
    context.putImageData(e.state, 0, 0);
  }
}
document.getElementById('file').addEventListener('change',function(e)
{
    clearCanvas();
    var temp = URL.createObjectURL(e.target.files[0]);
    var image = new Image();
    image.src = temp;
    context.globalCompositeOperation = "source-over";
    image.addEventListener('load',function()
    {
        imageWidth = image.naturalWidth;
        imageHeight = image.naturalHeight;
        newImageWidth = imageWidth;
        newImageHeight = imageHeight;
        originalImageRatio = imageWidth/imageHeight;

        if(newImageWidth> newImageHeight && newImageWidth > 800)
        {
            newImageWidth = 800;
            newImageHeight = 800/originalImageRatio;
        }
        if((newImageWidth>= newImageHeight || newImageHeight > newImageWidth) && newImageHeight > 500)
        {
            newImageHeight = 500;
            newImageWidth = 500*originalImageRatio;
        }
        context.drawImage(image,0,0,newImageWidth,newImageHeight);
        URL.revokeObjectURL(temp);
    });
});



