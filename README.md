# Software Studio 2020 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | N         |


---

### How to use 

   我的網頁的上方是我的工具列，其中redo,undo分別做redo,ondo的功能，text點了之後
   ，再我底下的畫布點一處即可輸入文字，有font:標籤的兩個selector可分別選字形和字
   的大小，triabgle,rectangle,circle,點了之後，再在畫布上點即可與滑鼠指標與原先
   點的位置的距離畫出triabgle,rectangle,circle，SIZE1~SIZE4可以決定pen,eraser的
   大小，而C按下去後則可清除畫布，COLOR可以選pen的顏色，OPEN選了之後，可以把想
   上傳的圖片上傳到畫布上，eraser可以清除東西，pen可以畫上東西，DOWNLOAD可以把
   畫布上的圖儲存下來。

### Function description

    Decribe your bouns function and how to use it.

### Gitlab page link

    https://107062332.gitlab.io/AS_01_WebCanvas 

### Others (Optional)

    Anythinh you want to say to TAs.

<style>
table th{
    width: 100%;
}
</style>